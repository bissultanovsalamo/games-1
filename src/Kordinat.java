public class Kordinat {
    private static int player = 1;
    private boolean[] flag = new boolean[9];
    private static Win win = new Win();
    private static String player1 = "";
    private static String player2 = "";

    public String add(int x, int y) {
        if (player == 1 && !player1.equals("Победил игрок №-1") && !player2.equals("Победил игрок №-2")) {

            if (x > 0 && x <= 112 && y > 0 && y <= 91 && !flag[0]) {
                player = 2;
                int i = 0;
                int j = 0;
                flag[0] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 0 && x <= 112 && y > 91 && y <= 184 && !flag[1]) {
                player = 2;
                int i = 0;
                int j = 1;
                flag[1] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 0 && x <= 112 && y > 184 && y <= 285 && !flag[2]) {
                player = 2;
                int i = 0;
                int j = 2;
                flag[2] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 112 && x <= 233 && y > 0 && y <= 91 && !flag[3]) {
                player = 2;
                int i = 1;
                int j = 0;
                flag[3] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 112 && x <= 233 && y > 91 && y <= 184 && !flag[4]) {
                player = 2;
                int i = 1;
                int j = 1;
                flag[4] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 112 && x <= 233 && y > 184 && y <= 285 && !flag[5]) {
                player = 2;
                int i = 1;
                int j = 2;
                flag[5] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 233 && x <= 335 && y > 0 && y <= 91 && !flag[6]) {
                player = 2;
                int i = 2;
                int j = 0;
                flag[6] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 233 && x <= 335 && y > 91 && y <= 184 && !flag[7]) {
                player = 2;
                int i = 2;
                int j = 1;
                flag[7] = true;
                player1 = win.winX(i, j);
                return player1;
            } else if (x > 233 && x <= 335 && y > 184 && y <= 285 && !flag[8]) {
                player = 2;
                int i = 2;
                int j = 2;
                flag[8] = true;
                player1 = win.winX(i, j);
                return player1;
            }

            return "Ход игока №-1!!!";
        }
        if (player == 2 && !player2.equals("Победил игрок №-2") && !player1.equals("Победил игрок №-1")) {


            if (player == 2) {
                if (x > 0 && x <= 112 && y > 0 && y <= 91 && !flag[0]) {
                    player = 1;
                    int i = 0;
                    int j = 0;
                    flag[0] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 0 && x <= 112 && y > 91 && y <= 184 && !flag[1]) {
                    player = 1;
                    int i = 0;
                    int j = 1;
                    flag[1] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 0 && x <= 112 && y > 184 && y <= 285 && !flag[2]) {
                    player = 1;
                    int i = 0;
                    int j = 2;
                    flag[2] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 112 && x <= 233 && y > 0 && y <= 91 && !flag[3]) {
                    player = 1;
                    int i = 1;
                    int j = 0;
                    flag[3] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 112 && x <= 233 && y > 91 && y <= 184 && !flag[4]) {
                    player = 1;
                    int i = 1;
                    int j = 1;
                    flag[4] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 112 && x <= 233 && y > 184 && y <= 285 && !flag[5]) {
                    player = 1;
                    int i = 1;
                    int j = 2;
                    flag[5] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 233 && x <= 335 && y > 0 && y <= 91 && !flag[6]) {
                    player = 1;
                    int i = 2;
                    int j = 0;
                    flag[6] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 233 && x <= 335 && y > 91 && y <= 184 && !flag[7]) {
                    player = 1;
                    int i = 2;
                    int j = 1;
                    flag[7] = true;
                    player2 = win.win0(i, j);
                    return player2;
                } else if (x > 233 && x <= 335 && y > 184 && y <= 285 && !flag[8]) {
                    player = 1;
                    int i = 2;
                    int j = 2;
                    flag[8] = true;
                    player2 = win.win0(i, j);
                    return player2;
                }

                return "Ход игока №-2!!!";
            }
        }
        return "";


    }


    public void flagg() {
        for (int i = 0; i < flag.length; i++) {
            flag[i] = false;
            player = 1;
            player1 = "";
            player2 = "";
        }
        win.restart();
    }
}

