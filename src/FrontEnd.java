import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;

public class FrontEnd  {
    private static JButton buttonStart;
    private static JButton buttonRestart;
    private static JTextField jTextField;
    private static JPanel panelJText;
    private static JPanel panel;
    private static JPanel panelButton;
    private static MyCanvas myCanvas = new MyCanvas();
    private static Kordinat kordinat = new Kordinat();
    private static int go = 1;
    private static boolean[] flag = new boolean[9];
    private static Win win = new Win();
    private static String move = "Ход игрока №-1!!!";
    private static Font font = new Font("Viva", Font.BOLD, 120);
    private  static Image img;
    private static  int count  = 2;
    public static void main(String[] args) {
        for (int i = 0; i < win.getArr().length; i++) {
            for (int j = 0; j < win.getArr().length; j++) {
                win.getArr()[i][j] = "-";
            }
        }

        JFrame frame = new JFrame();
        frame.setTitle("Games");
        frame.setSize(306, 482);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        img = Toolkit.getDefaultToolkit().getImage("src/image/photo_2020-04-26_20-40-28.jpg");
        System.out.println("Salamo");



        panel = new JPanel(new GridLayout());
        panel.add(myCanvas);
        frame.add(panel, BorderLayout.CENTER);
        panelJText = new JPanel(new GridLayout(0, 1));
        panelJText.add(jTextField = new JTextField(move,20));
        frame.add(panelJText, BorderLayout.NORTH);
        panelButton = new JPanel(new GridLayout(2, 1));
        buttonRestart = new JButton("Рестарт");
        buttonRestart.setBounds(20, 20, 20, 20);
        panelButton.add(buttonRestart);
        buttonStart = new JButton("Выход");
        buttonStart.setBounds(20, 0, 20, 20);
        panelButton.add(buttonStart);
        frame.add(panelButton, BorderLayout.PAGE_END);
        buttonRestart.addActionListener(new MyLisner());
        buttonStart.addActionListener(new MyLisner());
        myCanvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int y = e.getX();
                int x = e.getY();
                System.out.println("X=" + y);
                System.out.println("Y=" + x);
                myCanvas.repaint();
                move = kordinat.add(x, y);
                jTextField.setText(move);
            }
        });


        frame.setVisible(true);
    }

    static class MyLisner implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(buttonRestart)) {
                kordinat.flagg();
                move = "Ход игрока №-1!!!";
                jTextField.setText(move);
                if(count == 1) {
                    img = Toolkit.getDefaultToolkit().getImage("src/image/photo_2020-04-26_20-40-28.jpg");
                    count = 2;
                }else if(count == 2) {
                    img = Toolkit.getDefaultToolkit().getImage("src/image/photo_2020-04-26_20-57-48.jpg");
                    count = 3;
                }else if(count == 3){
                    img = Toolkit.getDefaultToolkit().getImage("src/image/photo_2020-04-26_23-46-18.jpg");
                    count = 1;
                }
                myCanvas.repaint();
            } else if (e.getSource().equals(buttonStart)) {
                System.exit(0);
            }
        }
    }

    static class MyCanvas extends JComponent {


        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
           g2.drawImage(img, 0,0,300,400, null);
            int height = 90;
            for (int i = 0; i < 2; i++) {
                g2.draw(new Line2D.Double(height, 400, height, 0));
                height += 95;
            }
            int wight = 113;
            for (int i = 0; i < 2; i++) {
                g2.draw(new Line2D.Double(0, wight, 400, wight));
                wight += 122;
            }
            if(go == 1)  {
                if (win.getArr()[0][0].equals("X") ) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 5, 100);

                }
                if (win.getArr()[0][1].equals("X")  ) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 100, 100);

                }
                if (win.getArr()[0][2].equals("X")) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 195, 100);

                }
                if (win.getArr()[1][0].equals("X")) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 5, 220);

                }
                if (win.getArr()[1][1].equals("X")) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 100, 220);

                }
                if (win.getArr()[1][2].equals("X") ) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 195, 220);

                }
                if (win.getArr()[2][0].equals("X")) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 5, 340);

                }
                if (win.getArr()[2][1].equals("X") ) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 100, 340);

                }
                if (win.getArr()[2][2].equals("X")) {
                    g2.setColor(Color.RED);
                    g2.setFont(font);
                    g2.drawString("X", 195, 340);

                }
                go = 2;

            }
              if(go == 2 ){
                if (win.getArr()[0][0].equals("0")) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 10, 100);

                }
                if (win.getArr()[0][1].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 105, 100);

                }
                if (win.getArr()[0][2].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 200, 100);

                }
                if (win.getArr()[1][0].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 10, 220);

                }
                if (win.getArr()[1][1].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 105, 220);

                }
                if (win.getArr()[1][2].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 200, 220);

                }
                if (win.getArr()[2][0].equals("0") ) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 10, 340);

                }
                if (win.getArr()[2][1].equals("0")) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 105, 340);

                }
                if (win.getArr()[2][2].equals("0")) {
                    g2.setColor(Color.blue);
                    g2.setFont(font);
                    g2.drawString("0", 200, 340);

                }
                go = 1;
              }
        }
    }
}


