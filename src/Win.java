public class Win extends Kordinat  {
    private static String[][] arr = new String[3][3];
    public static String[][] getArr() {
        return arr;
    }
    public static void setArr(String[][] arr) {
        Win.arr = arr;
    }

        private static String winX = "Победил игрок №-1";
        private static String win0 = "Победил игрок №-2";
    public String winX(int i, int j) {
        boolean flag = true;
            arr[i][j] = "X";
            if((arr[0][0] == "X" && arr[0][1] == "X" && arr[0][2] == "X")||(arr[0][0] == "X" && arr[1][0] == "X" && arr[2][0] == "X")||
                (arr[0][1] == "X" && arr[1][1] == "X" && arr[2][1] == "X")||(arr[0][2] == "X" && arr[1][2] == "X" && arr[2][2] == "X")||
                (arr[1][0] == "X" && arr[1][1] == "X" && arr[1][2] == "X")||(arr[2][0] == "X" && arr[2][1] == "X" && arr[2][2] == "X")||
                (arr[0][0] == "X" && arr[1][1] == "X" && arr[2][2] == "X")||(arr[0][2] == "X" && arr[1][1] == "X" && arr[2][0] == "X")){

        return winX;
        }else {

            for (int k = 0; k < arr.length ; k++) {
                for (int l = 0; l < arr.length; l++) {
                    if (arr[k][l].equals("-")) {
                        flag = false;
                        break;
                    }
                }
            }
                if(flag){
                    return "Ничья!!!";
                }
            }
            return "Ход игока №-2!!!";
    }
    public String win0(int i, int j) {
        arr[i][j] = "0";
        if((arr[0][0] == "0" && arr[0][1] == "0" && arr[0][2] == "0")||(arr[0][0] == "0" && arr[1][0] == "0" && arr[2][0] == "0")||
                (arr[0][1] == "0" && arr[1][1] == "0" && arr[2][1] == "0")||(arr[0][2] == "0" && arr[1][2] == "0" && arr[2][2] == "0")||
                (arr[1][0] == "0" && arr[1][1] == "0" && arr[1][2] == "0")||(arr[2][0] == "0" && arr[2][1] == "0" && arr[2][2] == "0")||
                (arr[0][0] == "0" && arr[1][1] == "0" && arr[2][2] == "0")||(arr[0][2] == "0" && arr[1][1] == "0" && arr[2][0] == "0")){
            return win0;
        }
        return "Ход игока №-1!!!";
    }

    public void restart() {
        for (int k = 0; k <arr.length; k++) {
            for (int l = 0; l <arr.length ; l++) {
               arr[k][l] = "-";

            }

        }
    }
}
